let message = "Hello World";
console.log(message);

let firstName = "John";
let lastName = "Smith";
let age = 30;

console.log(`First Name: ${firstName}`);
console.log(`Last Name: ${lastName}`);
console.log(`Age: ${age}`);

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];

console.log(`Hobbies:`);
console.log(hobbies);

let WorkAddress = {
	houseNumber: 32,
	streetName: "Washington",
	cityName: "Lincoln",
	stateName: "Nebraska"
}

console.log(`Work Address:`);
console.log(WorkAddress);

function printUserinfo(firstName, lastName, age, hobbies, WorkAddress){
	console.log(`${firstName} ${lastName} is ${age} years old`);
	console.log(`This was printed inside printUserinfo function:`);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log(`This was printed inside printUserinfo function:`);
	console.log("Work Address:");
	console.log(WorkAddress);
}

printUserinfo(firstName, lastName, age, hobbies, WorkAddress);



function returnFinal(){
	return true
}

let isMarried = returnFinal();
console.log(`The value of isMarried is: ${isMarried}`);
